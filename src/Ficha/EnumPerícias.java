/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ficha;

/**
 *
 * @author IMB5100
 */
public enum EnumPerícias {
    Acrobacia ("Acrobacia"),
    Adestrar_animais("Adestrar Animais"),
    Atletismo("Atletismo"),
    Cavalgar("Cavalgar"),
    Conhecimento("Conhecimento");
    
    private String numPerícias;

    private EnumPerícias(String numPerícias) {
        this.numPerícias = numPerícias;
    }

    public String getNumPerícias() {
        return numPerícias;
    }

    public void setNumPerícias(String numPerícias) {
        this.numPerícias = numPerícias;
    }


    
}
