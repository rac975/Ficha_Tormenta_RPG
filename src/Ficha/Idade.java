/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ficha;

import Classes.EnumClasses;
import java.util.Random;

public class Idade {

    private int penalidade = 0, aux;
    final int IdadeBase = 15, Maturidade = 35, Meia_Idade = 50, Velhice = 70;

    public Idade() {
    }

    Random gerador = new Random();

    public int calcIdade(int opClasse, int opRaca) {

        switch (opRaca) {
            case 1:
                aux = 1;
                break;
            case 2:
                aux = 2;
                break;
            case 3:
                aux = 3;
                break;
            case 6:
                aux = 6;
                break;
            case 10:
                aux = 10;
                break;
        }

        if (opClasse == 1 || opClasse == 5 || opClasse == 7 || opClasse == 13) {
            return ((IdadeBase + (gerador.nextInt(4) + 1)) * aux);
        } else if (opClasse == 2 || opClasse == 6 || opClasse == 10 || opClasse == 11) {
            return ((IdadeBase + (gerador.nextInt(6) + 1)) * aux);
        } else if (opClasse == 3 || opClasse == 4 || opClasse == 8 || opClasse == 9 || opClasse == 12) {
            return ((IdadeBase + (gerador.nextInt(6) + 1) + (gerador.nextInt(6) + 1)) * aux);
        }
        return 0;
    }

    public void PenalidadeIdade(Personagem personagem, Resistencia resistencia, int penalidade) {

        switch (personagem.getIdade()) {
            case Velhice:
                penalidade--;

            case Meia_Idade:
                penalidade--;

            case Maturidade:
                penalidade--;
                break;
        }

        personagem.addFOR(penalidade);
        personagem.addDES(penalidade, resistencia, personagem);
        personagem.addCON(penalidade, resistencia, personagem);
        personagem.addINT(penalidade * (-1));
        personagem.addSAB(penalidade * (-1), resistencia, personagem);
        personagem.addCAR(penalidade * (-1));
    }

}
