package Ficha;
import java.util.ArrayList;

public class Perícias {

	private int Grad, NGrad;
	private int Total, qtdPericia;
	private EnumPerícias enumPericia;
	
	
	public Perícias(Personagem p) {
		setGrad(p);
		setNGrad(p);
		setQtdPericia(p);
	}

        public EnumPerícias getEnumPericia() {
            return enumPericia;
        }

        public void setEnumPericia(EnumPerícias enumPericia) {
            this.enumPericia = enumPericia;
        }
	
	public void setGrad(Personagem p){
		Grad = p.getNivel() + 3;
	}
	
	public int getGrad(){
		return Grad;
	}
	
	public void setNGrad(Personagem p){
		Grad = p.getNivel() + 3;
	}
	
	public int getNGrad(){
		return NGrad;
	}
	
	public void setTotal(int paramTotal) {
		Total = paramTotal;
	}
	
	public int getTotal(){	
		return Total;
	}

	public void addOutros(int pOutros) {
		Total = getTotal() + pOutros;
	}
	
	public int getQtdPericiaa(){
		return qtdPericia;
	}
	
	public void setQtdPericia(Personagem p){
		qtdPericia = p.getModINT();
	}
	
	public void addQtdPericia(int pOutro){
		qtdPericia = getQtdPericiaa() + pOutro;
	}
        
        /*
        para bonus em diferentes ramificações da perícia, 
        fazer uma pergunta se quer adicionar o bônus com descrição do bônus!
        
        Ex: Deseja adicionar um Bônus de +2 em em seu testes de Ofício(Alvenaria)?
            Descrição: +2 em testes de perícias para assuntos relacionados a pedra e metal, 
            apenas para perícias com Inteligência ou Sabedoria como habilidade-chave.
        
        P.S: Fazer o mesmo com Resistencias.
        
        */
}
