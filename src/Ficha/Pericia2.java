/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ficha;

/**
 *
 * @author rafa_
 */
public class Pericia2 {
    private int Grad, NGrad, id;
    private int Total, qtdPericia;
    private String nome;

    public Pericia2() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public int getGrad() {
        return Grad;
    }

    public void setGrad(Personagem p) {
        this.Grad = p.getNivel() + 3;
    }

    public int getNGrad() {
        return NGrad;
    }

    public void setNGrad(int NGrad) {
        this.NGrad = NGrad;
    }

    public int getTotal() {
        return Total;
    }

    public void setTotal(int Total) {
        this.Total = Total;
    }

    public int getQtdPericia() {
        return qtdPericia;
    }

    public void setQtdPericia(int qtdPericia) {
        this.qtdPericia = qtdPericia;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public void acrobacia(Personagem p){
        setNome("Acrobacia");
        setGrad(p);
        setId(1);
    }
    
    public void adestrarAnimais(Personagem p){
        setNome("Adestrar Animais");
        setGrad(p);
        setId(2);
    }
    
    public void atletismo(Personagem p){
        setNome("Atletismo");
        setGrad(p);
        setId(3);
    }
    
    public void atuacao(Personagem p){
        setNome("Atuação");
        setGrad(p);
        setId(4);
    }
    
}
