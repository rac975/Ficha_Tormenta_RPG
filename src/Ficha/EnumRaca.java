/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ficha;

public enum EnumRaca {
    Vazio(0),
    Anão(1),
    Elfo(2),
    Goblin(3),
    Halfling(4),
    Humano(5),
    Lefou(6),
    Minotauro(7),
    Qareen(8);

    private int EscolhaRaca;

    private EnumRaca(int EscolhaRaca) {
        this.EscolhaRaca = EscolhaRaca;
    }

    public int getEscolhaRaca() {
        return EscolhaRaca;
    }
}
