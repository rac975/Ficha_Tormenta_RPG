package Ficha;
import java.util.ArrayList;
import Ficha.Pericia2;
/**
 *
 * @author IMB5100
 */
public class RepositoryPerícias {
    private ArrayList<Pericia2> repositoryPericias;
    private EnumPerícias enumPerícias;
    
    public RepositoryPerícias() {
    }
    
    public void addPerícia(Pericia2 p){
        repositoryPericias.add(p);
    }
    public void removePerícia(Pericia2 p){
        repositoryPericias.remove(p);
    }
    public Pericia2 getPericia(int id, Pericia2 p){
        
        return p;
    }
    public void initializePericias(Pericia2 peri, Personagem perso){
        peri.acrobacia(perso);
        addPerícia(peri);
        peri.adestrarAnimais(perso);
        addPerícia(peri);
        peri.atletismo(perso);
        addPerícia(peri);
        peri.atuacao(perso);
        addPerícia(peri);
    }
}
