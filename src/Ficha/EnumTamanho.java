/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ficha;

/**
 *
 * @author rafa_
 */
public enum EnumTamanho {
    
    Vazio(0),
    Infimo(1),
    Diminuto(2),
    Minimo(3),
    Pequeno(4),
    Medio(5),
    Grande(6),
    Enorme(7),
    Descomunal(8),
    Colossal(9);

    public int numTamanho;

    EnumTamanho(int numberTamanho) {
        numberTamanho = numTamanho;
    }

    public int getNumTamanho(int raca) {
        return numTamanho;
    }
    

    public void setNumTamanho(int numTamanho) {
        this.numTamanho = numTamanho;
    }
}
