/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ficha;

import java.util.Random;

public class Dados {

    private int ResultadoDado;
    Random dado = new Random();

    public Dados() {
    }

    public int getResultadoDado() {
        return ResultadoDado;
    }

    public void setResultadoDado(int ResultadoDado) {
        this.ResultadoDado = ResultadoDado;
    }

    public int d4() {
        setResultadoDado((dado.nextInt(4) + 1));
        return ResultadoDado;
    }

    public int d6() {
        setResultadoDado((dado.nextInt(6) + 1));
        return ResultadoDado;
    }

    public int d8() {
        setResultadoDado((dado.nextInt(8) + 1));
        return ResultadoDado;
    }

    public int d10() {
        setResultadoDado((dado.nextInt(10) + 1));
        return ResultadoDado;
    }

    public int d12() {
        setResultadoDado((dado.nextInt(12) + 1));
        return ResultadoDado;
    }

    public int d20() {
        setResultadoDado((dado.nextInt(20) + 1));
        return ResultadoDado;
    }

    public int d100() {
        setResultadoDado((dado.nextInt(100) + 1));
        return ResultadoDado;
    }

}
