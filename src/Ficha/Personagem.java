package Ficha;

import Classes.RepositorioClasse;
import Classes.Classe;
import Ficha.Idade;
import Ficha.Habilidades;

public class Personagem extends Habilidades {


    public String nomeDoPersonagem;
    public String nomeJogador;
    public int Nivel; // ENUM 1 - 20
    private int IdadeClasse, IdadeRaca;
    public int idade; //Calculado por raça
    private int PV;
    public int modTamanho; // Depende da Raça
    public int Deslocamento; // Depende da Raça e Armadura
    private int ClasseArmadura;
    private Resistencia resistencia;
    private int Total;
    private int bba;
    private Classe classes;

    public Personagem() {
        this.Nivel = 1;
        setFOR(10);
        setDES(10);
        setCON(10);
        setINT(10);
        setSAB(10);
        setCAR(10);
    }

    public Resistencia getResistencia() {
        return resistencia;
    }

    public void setResistencia(Resistencia resistencia) {
        this.resistencia = resistencia;
    }
    
    public int getFORTP(){
        return resistencia.getFORT();
    }
    
    public int getVONP(){
        return resistencia.getVON();
    }

    public int getBba() {
        return bba;
    }

    public void setBba(int nivel) {
        this.bba = nivel;
    }
    
    public int totalClasseArmadura() {
        ClasseArmadura = 10 + (Nivel / 2) + getModDES(); // Falta implementar Armadura, escudo e Outros, al�m de penalidade
        return ClasseArmadura;
    }

    public int pontosDeHabilidadePorNivel() {
        int PontosExtras = Nivel / 2;
        return PontosExtras;
    }

    public String getNomeDoPersonagem() {
        return nomeDoPersonagem;
    }

    public void setNomeDoPersonagem(String nomeDoPersonagem) {
        this.nomeDoPersonagem = nomeDoPersonagem;
    }

    public String getNomeJogador() {
        return nomeJogador;
    }

    public void setNomeJogador(String nomeJogador) {
        this.nomeJogador = nomeJogador;
    }

    public int getNivel() {
        return Nivel;
    }

    public void setNivel(int nivel) {
        Nivel = nivel;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public int getTamanho() {
        return modTamanho;
    }

    public void setTamanho( int tamanho){
        this.modTamanho = tamanho;
    }
    
    public void setModTamanho(int tamanho) {
        switch(tamanho){
        /*Vazio(0),
    Infimo(1),
    Diminuto(2),
    Minimo(3),
    Pequeno(4),
    Medio(5),
    Grande(6),
    Enorme(7),
    Descomunal(8),
    Colossal(9);*/
            case 1:
                addClasseArmadura(8);
                setTamanho(8);
                //addFurtividade(16);
                //Deslocamento????
                break;
            case 2:
                addClasseArmadura(4);
                setTamanho(4);
                //addFurtividade(12);
                //Deslocamento????
                break;
            case 3:
               addClasseArmadura(2);
               setTamanho(2);
                //addFurtividade(8);
                //Deslocamento????
                break;
            case 4:
                addClasseArmadura(1);
                setTamanho(1);
                //addFurtividade(4);
                //Deslocamento????
                break;
            case 5:
                addClasseArmadura(0);
                setTamanho(0);
                //addFurtividade(0);
                //Deslocamento????
                break;
            case 6:
                addClasseArmadura(-1);
                setTamanho(-1);
                //addFurtividade(-4);
                //Deslocamento????
                break;
            case 7:
                addClasseArmadura(-2);
                setTamanho(-2);
                //addFurtividade(-8);
                //Deslocamento????
                break;
            case 8:
                addClasseArmadura(-4);
                setTamanho(-4);
                //addFurtividade(-12);
                //Deslocamento????
                break;
            case 9:
                addClasseArmadura(-8);
                setTamanho(-8);
                //addFurtividade(-16);
                //Deslocamento????
                break;
        }
    }

    public int getDeslocamento() {
        return Deslocamento;
    }

    public void setDeslocamento(int deslocamento) {
        Deslocamento = deslocamento;
    }

    public int getClasseArmadura() {
        return ClasseArmadura;
    }

    public int getPV() {
        return PV;
    }

    public void setPV(int PV) {
        this.PV = PV;
    }

    public int CalcPV(int pPv) {
        return pPv;
    }

    public void setClasseArmadura(int classeArmadura) {
        ClasseArmadura = classeArmadura;
    }
    
    public void addClasseArmadura(int n) {
        ClasseArmadura += n;
    }

    public int getIdadeClasse() {
        return IdadeClasse;
    }

    public void setIdadeClasse(int IdadeClasse) {
        this.IdadeClasse = IdadeClasse;
    }

    public int getIdadeRaca() {
        return IdadeRaca;
    }

    public void setIdadeRaca(int IdadeRaca) {
        this.IdadeRaca = IdadeRaca;
    }

    public int AgoraIdade(Idade idade, int IdadeRaca, int IdadeClasse) {
        return idade.calcIdade(IdadeClasse, IdadeRaca);
    }
    
    public void zerar(){ 
        setFOR(10);
        setDES(10);
        setCON(10);
        setINT(10);
        setSAB(10);
        setCAR(10);
    }

    //TODO Pedir a explica��o de INTERFACE
    //TODO Metodo Upar
    //TODO Colocar HP(vida) aqui e passar por referencia para classe guerreiro
    //TODO Fazer um decrementador de HP
    //TODO Fazer um decrementador de MP para classe conjuradoras
}
