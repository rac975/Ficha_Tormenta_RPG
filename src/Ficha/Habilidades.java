package Ficha;

public abstract class Habilidades {

    private int ModFOR;
    public int FOR;
    private int ModDES;
    public int DES;
    private int ModCON;
    public int CON;
    private int ModINT;
    public int INT;
    private int ModSAB;
    public int SAB;
    private int ModCAR;
    public int CAR;
    private int HAB;

    public Habilidades() {

    }

    public int CalcHabilidades(int pHAB) {
        if (pHAB < 0) {
            pHAB = 0;
        }
        HAB = (pHAB - 10) / 2;
        if (pHAB < 10) {
            HAB = (pHAB - 11) / 2;
        }
        return HAB;
    }

    public int getModFOR() {
        return ModFOR;
    }

    public void setModFOR() {
        ModFOR = CalcHabilidades(FOR);
    }

    public int getFOR() {
        return FOR;
    }

    public void setFOR(int fOR) {
        FOR = fOR;
    }

    public int getModDES() {
        return ModDES;
    }

    public void setModDES() {
        ModDES = CalcHabilidades(DES);
    }

    public int getDES() {
        return DES;
    }

    public void setDES(int dES) {
        DES = dES;
    }

    public int getModCON() {
        return ModCON;
    }

    public void setModCON() {
        ModCON = CalcHabilidades(CON);
    }

    public int getCON() {
        return CON;
    }

    public void setCON(int cON) {
        CON = cON;
    }

    public int getModINT() {
        return ModINT;
    }

    public void setModINT() {
        ModINT = CalcHabilidades(INT);
    }

    public int getINT() {
        return INT;
    }

    public void setINT(int iNT) {
        INT = iNT;
    }

    public int getModSAB() {
        return ModSAB;
    }

    public void setModSAB() {
        ModSAB = CalcHabilidades(SAB);
    }

    public int getSAB() {
        return SAB;
    }

    public void setSAB(int sAB) {
        SAB = sAB;
    }

    public int getModCAR() {
        return ModCAR;
    }

    public void setModCAR() {
        ModCAR = CalcHabilidades(CAR);
    }

    public int getCAR() {
        return CAR;
    }

    public void setCAR(int cAR) {
        CAR = cAR;
    }

    public void addFOR(int paramAddFOR) {
        FOR = getFOR() + paramAddFOR;
        CalcHabilidades(FOR);
        setModFOR();
    }

    public void addDES(int paramAddDES, Resistencia r, Personagem p) {
        DES = getDES() + paramAddDES;
        CalcHabilidades(DES);
        setModDES();
        r.setREF(p,0);
    }

    public void addCON(int paramAddCON, Resistencia r, Personagem p) {
        CON = getCON() + paramAddCON;
        CalcHabilidades(CON);
        setModCON();
        r.setFORT(p, 0);
    }

    public void addINT(int paramAddINT) {
        INT = getINT() + paramAddINT;
        CalcHabilidades(INT);
        setModINT();
    }

    public void addSAB(int paramAddSAB, Resistencia r, Personagem p) {
        SAB = getSAB() + paramAddSAB;
        CalcHabilidades(SAB);
        setModSAB();
        r.setVON(p,0);
    }

    public void addCAR(int paramAddCAR) {
        CAR = getCAR() + paramAddCAR;
        CalcHabilidades(CAR);
        setModCAR();
    }

}
