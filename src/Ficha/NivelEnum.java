/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ficha;

/**
 *
 * @author IMB5100
 */
public enum NivelEnum {
    NIVEL1 (1),
    NIVEL2 (2),
    NIVEL3 (2), 
    NIVEL4 (4), 
    NIVEL5 (5), 
    NIVEL6 (6), 
    NIVEL7 (7), 
    NIVEL8 (8), 
    NIVEL9 (9), 
    NIVEL10 (10), 
    NIVEL11 (11), 
    NIVEL12 (12), 
    NIVEL13 (13), 
    NIVEL14 (14), 
    NIVEL15 (15), 
    NIVEL16 (16), 
    NIVEL17 (17), 
    NIVEL18 (18), 
    NIVEL19 (19), 
    NIVEL20 (20);
    
    public int valorNivel;
    
    NivelEnum(int valor) {
            valorNivel = valor;
    }
    
    public void escolherNivel(int valorNvl){
        if(valorNivel > 0 && valorNivel <= 20){
            this.valorNivel = valorNvl;
        }
    }
}
