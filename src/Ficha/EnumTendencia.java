/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ficha;

/**
 *
 * @author rafa_
 */
public enum EnumTendencia {
    
    Leal_e_Bom(1),
    Leal_e_Neutro(2),
    Leal_e_Mau(3),
    Neutro_e_Bom(4),
    Neutro(5),
    Neutro_e_Mau(6),
    Caotico_e_Bom(7),
    Caotico_e_Neutro(8),
    Caotico_e_Mau(9);
    
    private int opTendencia;
    
    EnumTendencia(int opTendencia){
        setOpTendencia(opTendencia);
    }
    
    public int getOpTendencia() {
        return opTendencia;
    }

    public void setOpTendencia(int opTendencia) {
        this.opTendencia = opTendencia;
    }
    
}
