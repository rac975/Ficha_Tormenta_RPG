package Ficha;

public class Resistencia {

	private int FORT;
	private int REF;
	private int VON;

	public Resistencia() {

	}

	public int getFORT() {
		return FORT;
	}

	public int getREF() {
		return REF;
	}

	public int getVON() {
		return VON;
	}

	public void setFORT(Personagem p, int outros) {
		FORT = (p.getNivel() / 2) + p.getModCON() + outros;
	}
	
	public void setREF(Personagem p, int outros) {
		REF = (p.getNivel() / 2) + p.getModDES() + outros;
	}

	public void setVON(Personagem p, int outros) {
		VON = (p.getNivel() / 2) + p.getModSAB() + outros;
	}

	public void addFORT(int outros) {
		FORT = getFORT() + outros;
	}

	public void addREF(int outros) {
		REF = getREF() + outros;
	}

	public void addVON(int outros) {
		VON = getVON() + outros;
	}
 
        public int BonusResistencia(int paramBonus, int paramResistencia){
            return (paramBonus + paramResistencia);
        }
        
        public int TesteResistencia(int paramResistencia, int valorDado){
            return (paramResistencia + valorDado);
        }

}
