/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ficha;

/**
 *
 * @author rafa_
 */
public enum EnumPericias {

    ACROBACIA("Acrobacia", 2),
    ADESTRAR_ANIMAIS("Adrestar Animais", 6),
    ATLETISMO("Atletismo", 1),
    ATUACAO("Atuação", 6),
    CAVALGAR("Cavalgar", 2),
    CONHECIMENTO("Conhecimento", 4),
    CURA("Cura", 5),
    DIPLOMACIA("Diplomacia", 6),
    ENGANAÇÃO("Enganação", 6),
    FURTIVIDADE("Furtividade", 2),
    INDENTIFICAR_MAGIA("Identificar Magia", 4),
    INICIATIVA("Iniciativa", 2),
    INTIMIDACAO("Intimidação", 6),
    INTUICAO("Intuição", 5),
    LADINAGEM("Ladinagem", 2),
    OBTER_INFORMACAO("Obter Informação", 6),
    OFICIO("Ofício", 4),
    PERCEPCAO("Percepção", 5),
    SOBREVIVENCIA("Sobrevivência", 5);

    /*
    HAB_FOR = 1;
    HAB_DES = 2;
    HAB_CON = 3;
    HAB_INT = 4;
    HAB_SAB = 5;
    HAB_CAR = 6; 
     */
    private String nome;
    private int hab;
    private boolean treinda;

    EnumPericias(String nome, int hab) {
        this.nome = nome;
        this.hab = hab;
    }

    public String getNome() {
        return nome;
    }

    @Override
    public String toString() {
        return this.nome;
    }

    public int getHab() {
        return hab;
    }

    public boolean isTreinda() {
        return treinda;
    }

    public void setTreinda(boolean treinda) {
        this.treinda = treinda;
    }
}
