/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ficha;

/**
 *
 * @author rafa_
 */
public class Combate {

    private int corpoAcorpo, aDistancia;

    public int getCorpoAcorpo() {
        return corpoAcorpo;
    }

    public void setCorpoAcorpo(int corpoAcorpo) {
        this.corpoAcorpo = corpoAcorpo;
    }

    public int getaDistancia() {
        return aDistancia;
    }

    public void setaDistancia(int aDistancia) {
        this.aDistancia = aDistancia;
    }

    public int corpoAcorpo(Personagem p) {
        return corpoAcorpo = p.getBba() + p.getModFOR() + p.getTamanho();
    }

    public int aDistancia(Personagem p) {
        return aDistancia = p.getBba() + p.getModDES() + p.getTamanho();
    }

    public void addCaC(int outros) {
        corpoAcorpo += outros;
    }

    public void addAdistancia(int outros) {
        aDistancia += outros;
    }

}
