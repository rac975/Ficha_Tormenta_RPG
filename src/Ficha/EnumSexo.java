/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ficha;

public enum EnumSexo {
    
    Feminino(false), Masculino(true);
   
    private boolean SexoOP;

    public boolean isSexoOP() {
        return SexoOP;
    }

    public void setSexoOP(boolean SexoOP) {
        this.SexoOP = SexoOP;
    }
    
    EnumSexo(boolean op){
        SexoOP = op;
    }
    
    
    
}
