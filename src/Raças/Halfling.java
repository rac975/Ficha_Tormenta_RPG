/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Raças;

import Ficha.Personagem;
import Ficha.Resistencia;

/**
 *
 * @author rafa_
 */
public class Halfling extends Raça{

    public Halfling() {
    }

    public void novaRaça(Personagem p, Resistencia r) {
        p.addFOR(-2);
        p.addDES(4, r, p);
        p.addCAR(2);
        p.setIdadeRaca(2);
    }

    public void desfazer(Personagem p, Resistencia r) {
        p.addFOR(2);
        p.addDES(-4, r, p);
        p.addCAR(-2);
    }
}
