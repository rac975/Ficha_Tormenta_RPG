/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Raças;

import java.util.Scanner;
import Ficha.Personagem;
import Ficha.Resistencia;
import Talentos.TalentosDeCombate;
import Ficha.Perícias;

public class Elfo {

    public Elfo() {
    }

    public void novaRaça(Personagem p, Resistencia r) {
        p.addDES(4, r, p);
        p.addINT(2);
        p.addCON(-2, r, p);
        p.setIdadeRaca(10);
    }

    public void desfazer(Personagem p, Resistencia r) {
        p.addDES(-4, r, p);
        p.addINT(-2);
        p.addCON(2, r, p);
    }
}
