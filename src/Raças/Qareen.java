/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Raças;

import Ficha.Personagem;
import Ficha.Resistencia;

/**
 * @author lincoln
 */
public class Qareen extends Raça{

    public Qareen() {
    }

    public void novaRaça(Personagem p, Resistencia r) {
        p.addCAR(4);
        p.addINT(2);
        p.addSAB(-2, r, p);
        p.setIdadeRaca(6);
    }

    public void desfazer(Personagem p, Resistencia r) {
        p.addCAR(-4);
        p.addINT(-2);
        p.addSAB(2, r, p);
    }
}
