/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Raças;

import Ficha.Personagem;
import Ficha.Perícias;
import Ficha.Resistencia;
import View.EscolhaHabilidade;
import View.FichaRPG;

/**
 *
 * @author rafa_
 */
public class Lefou extends Raça{

    public Lefou() {
    }
    public void ChamarTela(Perícias pe, Personagem personagem, Resistencia resistencia, FichaRPG back, int raca) {
        EscolhaHabilidade EscHab = new EscolhaHabilidade(personagem, resistencia, pe, back, raca);
        EscHab.setSize(250, 200);
        EscHab.setVisible(true);
        pe.addQtdPericia(2);
        personagem.setIdadeRaca(1);
    }
    
    public void desfazerLefou(Perícias pe, Personagem personagem, Resistencia r, int op1, int op2) {
        switch (op1) {
            case 1:
                personagem.addFOR(-2);
                break;
            case 2:
                personagem.addDES(-2, r, personagem);
                break;
            case 3:
                personagem.addCON(-2, r, personagem);
                break;
            case 4:
                personagem.addINT(-2);
                break;
            case 5:
                personagem.addSAB(-2, r, personagem);
                break;
        }

        switch (op2) {
            case 1:
                personagem.addFOR(-2);
                break;
            case 2:
                personagem.addDES(-2, r, personagem);
                break;
            case 3:
                personagem.addCON(-2, r, personagem);
                break;
            case 4:
                personagem.addINT(-2);
                break;
            case 5:
                personagem.addSAB(-2, r, personagem);
                break;
        }
    }
}
