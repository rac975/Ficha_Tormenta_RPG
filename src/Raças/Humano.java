package Raças;

import java.util.Scanner;
import Ficha.Personagem;
import Ficha.Perícias;
import Ficha.Resistencia;
import Talentos.TalentosDeCombate;
import View.EscolhaHabilidade;
import View.FichaRPG;
import javax.swing.JOptionPane;

public class Humano extends Raça{


    public Humano() {

    }

    public void ChamarTela(Perícias pe, Personagem personagem, Resistencia resistencia, FichaRPG back, int raca) {
        EscolhaHabilidade EscHab = new EscolhaHabilidade(personagem, resistencia, pe, back, raca);
        EscHab.setSize(250, 200);
        EscHab.setVisible(true);
        pe.addQtdPericia(2);
        personagem.setIdadeRaca(1);
    }
    
    public void desfazerHumano(Perícias pe, Personagem personagem, Resistencia r, int op1, int op2) {
        switch (op1) {
            case 1:
                personagem.addFOR(-2);
                break;
            case 2:
                personagem.addDES(-2, r, personagem);
                break;
            case 3:
                personagem.addCON(-2, r, personagem);
                break;
            case 4:
                personagem.addINT(-2);
                break;
            case 5:
                personagem.addSAB(-2, r, personagem);
                break;
            case 6:
                personagem.addCAR(-2);
                break;
        }

        switch (op2) {
            case 1:
                personagem.addFOR(-2);
                break;
            case 2:
                personagem.addDES(-2, r, personagem);
                break;
            case 3:
                personagem.addCON(-2, r, personagem);
                break;
            case 4:
                personagem.addINT(-2);
                break;
            case 5:
                personagem.addSAB(-2, r, personagem);
                break;
            case 6:
                personagem.addCAR(-2);
                break;
        }
    }
}
