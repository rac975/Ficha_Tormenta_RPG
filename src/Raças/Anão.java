/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Raças;

import java.util.Scanner;
import Ficha.Personagem;
import Ficha.Resistencia;
import Talentos.TalentosDeCombate;
import Ficha.Perícias;
/**
 *
 * @author rafa_
 */
public class Anão extends Raça{
    public Anão() {
    }

    public void novaRaça(Personagem p, Resistencia r) {
        p.addCON(4, r, p);
        p.addDES(-2,r, p);
        p.addSAB(2, r, p);
        p.setIdadeRaca(3);
    }

    public void desfazer(Personagem p, Resistencia r) {
        p.addCON(-4, r, p);
        p.addDES(2,r, p);
        p.addSAB(-2, r, p);
    }
}