package Talentos;

import Ficha.Personagem;
import Ficha.Resistencia;

public class TalentosDeCombate extends TalentosGerais {
	
	public TalentosDeCombate(){	
	}
	
	public void FortitudeMaior(Resistencia r, Personagem p, int outros){
		setNomeTalento("Fortitude Maior");
                setNivelNecessario(0);
		r.setFORT(p, outros);
	}
	
	public void ReflexosRápidos(Resistencia r, Personagem p, int outros){
		setNomeTalento("Reflexos Rápidos");
                setNivelNecessario(0);
		r.addREF(2);
	}
	
	public void VontadeDeFerro(Resistencia r, Personagem p, int outros){
		setNomeTalento("Vontade de Ferro");
                setNivelNecessario(0);
		r.addVON(2);
	}
        
        public void FocoEmArma(){
            setNomeTalento("Foco em Arma");
            setNivelNecessario(0);
            //arma.TipoArma(); Tipo de Arma Ex: Espada Longa, Florete, Adaga, Arco...
            //arma.addBonusAtaque(1); +1 no acerto da arma
        }
        public void CascaGrossa(Personagem p){
            if(p.getCON() >= 13){
                setNomeTalento("Foco em Arma");
                setNivelNecessario(0);
                
            }
        }
}
