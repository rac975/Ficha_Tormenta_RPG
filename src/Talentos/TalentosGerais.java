package Talentos;

public class TalentosGerais {

    private String nomeTalento, Descrição;
    private int nivelNecessario;

    public int getNivelNecessario() {
        return nivelNecessario;
    }

    public void setNivelNecessario(int nivelNecessario) {
        this.nivelNecessario = nivelNecessario;
    }

    public String getDescrição() {
        return Descrição;
    }

    public void setDescrição(String descrição) {
        Descrição = descrição;
    }

    public String getNomeTalento() {
        return nomeTalento;
    }

    public void setNomeTalento(String nomeTalento) {
        this.nomeTalento = nomeTalento;
    }

}
