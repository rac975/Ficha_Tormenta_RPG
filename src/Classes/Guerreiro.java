package Classes;

import java.util.Scanner;

import Ficha.Personagem;
import Ficha.Perícias;
import Ficha.Resistencia;
import Talentos.TalentosDeCombate;
import Ficha.Idade;

public class Guerreiro extends Classe{

    private int op;
    private int vida;
    Scanner scanner = new Scanner(System.in);
    // private int qtdPericia;
    // private int qtdNRepeatPericias[];

    public Guerreiro(Personagem p, Perícias pe) {

        setBBA(p);
        pe.addQtdPericia(2);
    }

    @Override
    public void criarNovaClasse(Personagem p, Perícias pe, Resistencia r, TalentosDeCombate t, Idade i) {

        //t.FortitudeMaior(r, p);
        //r.setFORT(p);
        r.addFORT(2); // Ver Isso depois.
        p.setResistencia(r);
        setVida(p);
        p.setIdadeClasse(2);
        setBBA(p);
    }

    public void setBBA(Personagem p) {
        p.setBba(p.getNivel());
    }

    public int getVida(Personagem p) {
        setVida(p);
        return vida;
    }

    public void setVida(Personagem p) {
        if (p.getNivel() == 1) {
            p.setPV(p.getModCON() + 20);
        } else {
            p.setPV((p.getModCON() + 20) + ((p.getNivel() - 1) * (p.getModCON() + 5)));
        }
    }
    /*
	 * if (getNivel() % 2 == 0 || getNivel() == 1) {
	 * System.out.println("Tecnica de Luta"); // Implementar Metodo para //
	 * chamar Talentos de // Combate e colocar quando Upar
     */

}
