/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import Ficha.Idade;
import Ficha.Personagem;
import Ficha.Perícias;
import Ficha.Resistencia;
import Talentos.TalentosDeCombate;

/**
 *
 * @author rafa_
 */
public class Paladino extends Classe{

    private int vida;

    public Paladino(Personagem p, Perícias pe) {
        setBBA(p);
        pe.addQtdPericia(2);
    }

    @Override
    public void criarNovaClasse(Personagem p, Perícias pe, Resistencia r, TalentosDeCombate t, Idade i) {
        t.FortitudeMaior(r, p, 2);
        p.setResistencia(r);
        setVida(p);
        p.setIdadeClasse(2);
        setBBA(p);
    }

    public void setBBA(Personagem p) {
        p.setBba(p.getNivel());
    }

    public int getVida(Personagem p) {
        setVida(p);
        return vida;
    }

    public void setVida(Personagem p) {
        if (p.getNivel() == 1) {
            p.setPV(p.getModCON() + 20);
        } else {
            p.setPV((p.getModCON() + 20) + ((p.getNivel() - 1) * (p.getModCON() + 5)));
        }
    }
}
