/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

public enum EnumClasses {

    Vazio(0),
    Bárbaro(1), //1
    Bardo(2), //2
    Clérigo(3),//3
    Druida(4), //3
    Feiticeiro(5), //1
    Guerreiro(6), //2
    Ladino(7), //1
    Mago(8), //3
    Monge(9), //3
    Paladino(10), //2
    Ranger(11), //2
    Samurai(12), //3
    Swashbuckler(13); //1

    private final int EscolhaClasse;

    EnumClasses(int opClasse) {
        EscolhaClasse = opClasse;
    }

    public int getEscolharClasse() {
        return EscolhaClasse;
    }
}
