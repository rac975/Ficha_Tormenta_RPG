/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;
    
import java.util.ArrayList;

public class RepositorioClasse {
    
    private Classe classe = new Classe();
    
    private ArrayList<Classe> classes;

    public Classe getClasse() {
        return classe;
    }

    public void setClasse(Classe classe) {
        this.classe = classe;
    }
    
    //Monge 1/Swashbuckler 1/Nobre 8
    
    public ArrayList<Classe> getClasses() {
        return classes;
    }

    public void setClasses(ArrayList<Classe> classes) {
        this.classes = classes;
    }

    public void addClasse(Classe paramClasse) { //TODO o somatório dos niveis tem que ser igual ou menor que 20
        classes.add(paramClasse);
    }
}
