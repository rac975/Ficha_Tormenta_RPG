/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import Ficha.Idade;
import Ficha.Personagem;
import Ficha.Perícias;
import Ficha.Resistencia;
import Talentos.TalentosDeCombate;

/**
 *
 * @author rafa_
 */
public class Swashbuckler extends Classe{
    
    private int vida;

    public Swashbuckler(Personagem p, Perícias pe) {
        setBBA(p);
        pe.addQtdPericia(4);
    }

    @Override
    public void criarNovaClasse(Personagem p, Perícias pe, Resistencia r, TalentosDeCombate t, Idade i) {
        t.ReflexosRápidos(r, p, 2);
        p.setResistencia(r);
        setVida(p);
        p.setIdadeClasse(1);
        setBBA(p);
    }

    public void setBBA(Personagem p) {
        p.setBba(p.getNivel());
    }

    public int getVida(Personagem p) {
        setVida(p);
        return vida;
    }

    public void setVida(Personagem p) {
        if (p.getNivel() == 1) {
            p.setPV(p.getModCON() + 16);
        } else {
            p.setPV((p.getModCON() + 16) + ((p.getNivel() - 1) * (p.getModCON() + 4)));
        }
    }
    
}
