/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//TODO Implementar o restante das classes
//TODO Raça esta sobrescreevendo modificadores de Classe TUDO
//TODO Habilidade esta sobrescreevendo resistencia
//TODO Criar Perícias
//TODO Criar Talentos
//TODO Criar Magias
//TODO Combate
//TODO Equipamentos(Armas, Itens, Armaduras)
package View;

import Classes.Guerreiro;
import Ficha.EnumRaca;
import Ficha.EnumSexo;
import Classes.EnumClasses;
import Classes.Mago;
import Classes.Bardo;
import Classes.Bárbaro;
import Classes.Clerigo;
import Classes.Druida;
import Classes.Feiticeiro;
import Classes.Ladino;
import Classes.Monge;
import Classes.Paladino;
import Classes.Ranger;
import Classes.Samurai;
import Classes.Swashbuckler;
import Ficha.Combate;
import Ficha.Dados;
import Ficha.EnumTamanho;
import Ficha.Personagem;
import Ficha.Perícias;
import Ficha.Resistencia;
import Ficha.Idade;
import Ficha.Pericia2;
import Ficha.RepositoryPerícias;
import Raças.Humano;
import Raças.Elfo;
import Raças.Qareen;
import Raças.Anão;
import Raças.Lefou;
import Raças.Halfling;
import Raças.Minotauro;
import Raças.Goblin;
import View.EscolhaHabilidade;
import Talentos.TalentosDeCombate;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import Classes.Classe;
import com.sun.xml.internal.ws.encoding.SwACodec;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author lincoln
 */
public class FichaRPG extends javax.swing.JFrame {

    Personagem personagem = new Personagem();
    Combate combate = new Combate();
    RepositoryPerícias repPericia = new RepositoryPerícias();
    Pericia2 pericia = new Pericia2();
    Resistencia resistencia = new Resistencia();
    Perícias pericias = new Perícias(personagem);
    Classe classe;
    Classe guerreiro = new Guerreiro(personagem, pericias);
    Classe mago = new Mago(personagem, pericias);
    Classe bardo = new Bardo(personagem, pericias);
    Classe barbaro = new Bárbaro(personagem, pericias);
    Classe clerigo = new Clerigo(personagem, pericias);
    Classe druida = new Druida(personagem, pericias);
    Classe feiticeiro = new Feiticeiro(personagem, pericias);
    Classe ladino = new Ladino(personagem, pericias);
    Classe monge = new Monge(personagem, pericias);
    Classe paladino = new Paladino(personagem, pericias);
    Classe ranger = new Ranger(personagem, pericias);
    Classe samurai = new Samurai(personagem, pericias);
    Classe swashbuckler = new Swashbuckler(personagem, pericias);
    Humano humano = new Humano();
    Elfo elfo = new Elfo();
    Qareen qareen = new Qareen();
    Anão anão = new Anão();
    Halfling halfling = new Halfling();
    Minotauro minotauro = new Minotauro();
    Goblin goblin = new Goblin();
    Lefou lefou = new Lefou();
    Idade idade = new Idade();
    EscolhaHabilidade escolhahabilidade;
    TalentosDeCombate talentosC = new TalentosDeCombate();
    Dados dado = new Dados();
    public int opRaca_Anterior;
    private int op1, op2;
    private int modRaca;
    private int valoranteriorFOR = 0;
    private int valoranteriorDES = 0;
    private int valoranteriorCON = 0;
    private int valoranteriorINT = 0;
    private int valoranteriorSAB = 0;
    private int valoranteriorCAR = 0;
    private int Total = 15;
    private int racaOK = 0;

    public FichaRPG() {
        initComponents();
        NextPage.setVisible(false);
    }

    //TODO Fazer um controle para refresh a view (atualizar as labels)/Controller
    public FichaRPG(Personagem p) {
        //repPericia.initializePericias(pericia, p);
        initComponents();
        refreshView(p);
        valoranteriorFOR = personagem.getFOR();
        valoranteriorDES = personagem.getDES();
        valoranteriorCON = personagem.getCON();
        valoranteriorINT = personagem.getINT();
        valoranteriorSAB = personagem.getSAB();
        valoranteriorCAR = personagem.getCAR();
    }

    public int getTotal() {
        return Total;
    }

    public void setTotal(int Total) {
        this.Total = Total;
    }

    public void addTotal(int total) {
        int i = 0;
        setTotal(15);
        while (i != total) {
            Total++;
            i++;
        }
    }

    public int getOp1() {
        return op1;
    }

    public void setOp1(int op1) {
        this.op1 = op1;
    }

    public int getOp2() {
        return op2;
    }

    public void setOp2(int op2) {
        this.op2 = op2;
    }

    public void setarRacas() {

        if (opRaca_Anterior == 1) {
            anão.desfazer(personagem, resistencia);
        }
        if (opRaca_Anterior == 2) {
            elfo.desfazer(personagem, resistencia);
        }
        if (opRaca_Anterior == 3) {
            goblin.desfazer(personagem, resistencia);
        }
        if (opRaca_Anterior == 4) {
            halfling.desfazer(personagem, resistencia);
        }
        if (opRaca_Anterior == 5) {
            humano.desfazerHumano(pericias, personagem, resistencia, getOp1(), getOp2()); //Conversar com Lincoln e Marcos sobre isso.
        }
        if (opRaca_Anterior == 6) {
            lefou.desfazerLefou(pericias, personagem, resistencia, getOp1(), getOp2());
        }
        if (opRaca_Anterior == 7) {
            minotauro.desfazer(personagem, resistencia);
        }
        if (opRaca_Anterior == 8) {
            qareen.desfazer(personagem, resistencia);
        }
        setTotal(15);
    }

    public void refreshView(Personagem p) {
        personagem.setFOR(p.getFOR());
        personagem.setModFOR();

        personagem.setINT(p.getINT());
        personagem.setModINT();

        personagem.setDES(p.getDES());
        personagem.setModDES();

        personagem.setCON(p.getCON());
        personagem.setModCON();

        personagem.setSAB(p.getSAB());
        personagem.setModSAB();

        personagem.setCAR(p.getCAR());
        personagem.setModCAR();

        switch (ClasseBox.getSelectedIndex()) {//TODO Atualizar os modificadores de FORT, REF e VON de cada classe
            case 1:
                barbaro.setBBA(personagem);
                resistencia.setFORT(personagem, 2);
                resistencia.setVON(personagem, 0);
                resistencia.setREF(personagem, 0);
                break;
            case 2:
                bardo.setBBA(personagem);
                resistencia.setFORT(personagem, 2);
                resistencia.setVON(personagem, 2);
                resistencia.setREF(personagem, 0);
                break;
            case 3:
                clerigo.setBBA(personagem);
                resistencia.setFORT(personagem, 2);
                resistencia.setVON(personagem, 2);
                resistencia.setREF(personagem, 0);
                break;
            case 4:
                druida.setBBA(personagem);
                resistencia.setFORT(personagem, 2);
                resistencia.setVON(personagem, 2);
                resistencia.setREF(personagem, 0);
                break;
            case 5:
                feiticeiro.setBBA(personagem);
                resistencia.setVON(personagem, 2);
                resistencia.setFORT(personagem, 0);
                resistencia.setREF(personagem, 0);
                break;
            case 6:
                guerreiro.setBBA(personagem);
                resistencia.setFORT(personagem, 2);
                resistencia.setVON(personagem, 0);
                resistencia.setREF(personagem, 0);
                break;
            case 7:
                ladino.setBBA(personagem);
                resistencia.setREF(personagem, 2);
                resistencia.setFORT(personagem, 0);
                resistencia.setVON(personagem, 0);
                break;
            case 8:
                mago.setBBA(personagem);
                resistencia.setVON(personagem, 2);
                resistencia.setFORT(personagem, 0);
                resistencia.setREF(personagem, 0);
                break;
            case 9:
                monge.setBBA(personagem);
                resistencia.setFORT(personagem, 2);
                resistencia.setVON(personagem, 2);
                resistencia.setREF(personagem, 2);
                break;
            case 10:
                paladino.setBBA(personagem);
                resistencia.setFORT(personagem, 2);
                resistencia.setVON(personagem, 0);
                resistencia.setREF(personagem, 0);
                break;
            case 11:
                ranger.setBBA(personagem);
                resistencia.setFORT(personagem, 2);
                resistencia.setREF(personagem, 2);
                resistencia.setVON(personagem, 0);
                break;
            case 12:
                samurai.setBBA(personagem);
                resistencia.setFORT(personagem, 2);
                resistencia.setVON(personagem, 2);
                resistencia.setREF(personagem, 0);
                break;
            case 13:
                swashbuckler.setBBA(personagem);
                resistencia.setREF(personagem, 2);
                resistencia.setFORT(personagem, 0);
                resistencia.setVON(personagem, 0);
                break;
        }

        spinnerFOR.setValue(personagem.getFOR());
        spinnerDES.setValue(personagem.getDES());
        spinnerCON.setValue(personagem.getCON());
        spinnerINT.setValue(personagem.getINT());
        spinnerSAB.setValue(personagem.getSAB());
        spinnerCAR.setValue(personagem.getCAR());
        personagem.totalClasseArmadura();

        lblModFor.setText(Integer.toString(personagem.getModFOR()));
        lblModDes.setText(Integer.toString(personagem.getModDES()));
        lblModCon.setText(Integer.toString(personagem.getModCON()));
        lblModInt.setText(Integer.toString(personagem.getModINT()));
        lblModSab.setText(Integer.toString(personagem.getModSAB()));
        lblModCar.setText(Integer.toString(personagem.getModCAR()));

        lblFORT.setText(Integer.toString(resistencia.getFORT()));
        lblREF.setText(Integer.toString(resistencia.getREF()));
        lblVON.setText(Integer.toString(resistencia.getVON()));
        lblPV.setText(Integer.toString(personagem.getPV()));

        lblCA.setText(Integer.toString(personagem.getClasseArmadura()));
        lblCaCbba.setText(Integer.toString(p.getBba()));
        lblCaCmodFOR.setText(Integer.toString(p.getModFOR()));
        lblCaCTamanho.setText(Integer.toString(p.getTamanho()));
        lblCac.setText(Integer.toString(combate.corpoAcorpo(p)));
        lblAdist.setText(Integer.toString(combate.aDistancia(p)));
        lblDistBba.setText(Integer.toString(p.getBba()));
        lblDistModDes.setText(Integer.toString(p.getModDES()));
        lblDistModTam.setText(Integer.toString(p.getTamanho()));
        somaHabilidades();
        //spinnerFOR.setValue(Integer.toString(personagem.getFOR()));

    }

    public void parametros(EscolhaHabilidade escolhahabilidade, int op1, int op2) {
        this.escolhahabilidade = escolhahabilidade;
        setOp1(op1);
        setOp2(op2);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        NomePersonagem = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        NomeJogador = new javax.swing.JTextField();
        LblFOR = new javax.swing.JLabel();
        lblDES = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        lblModSab = new javax.swing.JLabel();
        lblModCar = new javax.swing.JLabel();
        lblModFor = new javax.swing.JLabel();
        lblModDes = new javax.swing.JLabel();
        lblModCon = new javax.swing.JLabel();
        lblModInt = new javax.swing.JLabel();
        NextPage = new javax.swing.JButton();
        RaçaBox = new javax.swing.JComboBox<>();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        lblREF = new javax.swing.JLabel();
        lblVON = new javax.swing.JLabel();
        lblPV = new javax.swing.JLabel();
        lblCA = new javax.swing.JLabel();
        lblFORT = new javax.swing.JLabel();
        NivelSpinner = new javax.swing.JSpinner();
        jLabel33 = new javax.swing.JLabel();
        ClasseBox = new javax.swing.JComboBox<>();
        jLabel31 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        jLabel35 = new javax.swing.JLabel();
        SexoBox = new javax.swing.JComboBox<>();
        jLabel36 = new javax.swing.JLabel();
        jLabel38 = new javax.swing.JLabel();
        lblDesc = new javax.swing.JLabel();
        jLabel40 = new javax.swing.JLabel();
        jLabel41 = new javax.swing.JLabel();
        TamanhoBox = new javax.swing.JComboBox<>();
        jComboBox1 = new javax.swing.JComboBox<>();
        Logotipo = new javax.swing.JLabel();
        ButtonRolarFORT = new javax.swing.JButton();
        lblTesteFORT = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        lblIdade = new javax.swing.JLabel();
        ButtonIdade = new javax.swing.JButton();
        spinnerFOR = new javax.swing.JSpinner();
        spinnerDES = new javax.swing.JSpinner();
        spinnerCON = new javax.swing.JSpinner();
        spinnerINT = new javax.swing.JSpinner();
        spinnerSAB = new javax.swing.JSpinner();
        spinnerCAR = new javax.swing.JSpinner();
        jCheckBoxAcrobacia = new javax.swing.JCheckBox();
        jCheckBoxAdestrar = new javax.swing.JCheckBox();
        jCheckBoxAtletismo = new javax.swing.JCheckBox();
        jCheckBoxAtuaca = new javax.swing.JCheckBox();
        jLabel1 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        lblCac = new javax.swing.JLabel();
        lblAdist = new javax.swing.JLabel();
        lblCaCbba = new javax.swing.JLabel();
        lblCaCmodFOR = new javax.swing.JLabel();
        lblCaCTamanho = new javax.swing.JLabel();
        lblDistBba = new javax.swing.JLabel();
        lblDistModDes = new javax.swing.JLabel();
        lblDistModTam = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setPreferredSize(new java.awt.Dimension(300, 300));
        setSize(new java.awt.Dimension(620, 550));

        NomePersonagem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NomePersonagemActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel2.setText("Nome");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel3.setText("Nome do jogador");

        LblFOR.setText("FOR");

        lblDES.setText("DES");

        jLabel6.setText("CON");

        jLabel7.setText("INT");

        jLabel8.setText("SAB");

        jLabel9.setText("CAR");

        lblModSab.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblModSab.setText("0");

        lblModCar.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblModCar.setText("0");

        lblModFor.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblModFor.setText("0");
        lblModFor.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                lblModForInputMethodTextChanged(evt);
            }
        });
        lblModFor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                lblModForKeyReleased(evt);
            }
        });

        lblModDes.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblModDes.setText("0");

        lblModCon.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblModCon.setText("0");

        lblModInt.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblModInt.setText("0");

        NextPage.setText("Next");
        NextPage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NextPageActionPerformed(evt);
            }
        });

        RaçaBox.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        RaçaBox.setModel(new javax.swing.DefaultComboBoxModel(EnumRaca.values()));
        RaçaBox.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                RaçaBoxMouseClicked(evt);
            }
        });
        RaçaBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RaçaBoxActionPerformed(evt);
            }
        });

        jLabel22.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel22.setText("PV");

        jLabel23.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel23.setText("CA");

        jLabel24.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel24.setText("FORT");

        jLabel25.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel25.setText("REF");

        jLabel26.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel26.setText("VON");

        lblREF.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblREF.setText("0");

        lblVON.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblVON.setText("0");

        lblPV.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblPV.setText("0");

        lblCA.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblCA.setText("0");

        lblFORT.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblFORT.setText("0");

        NivelSpinner.setModel(new javax.swing.SpinnerNumberModel(1, 1, 20, 1));
        NivelSpinner.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                NivelSpinnerStateChanged(evt);
            }
        });
        NivelSpinner.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                NivelSpinnerInputMethodTextChanged(evt);
            }
        });

        jLabel33.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel33.setText("Nivel");

        ClasseBox.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        ClasseBox.setModel(new javax.swing.DefaultComboBoxModel(EnumClasses.values()));
        ClasseBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ClasseBoxActionPerformed(evt);
            }
        });

        jLabel31.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel31.setText("Raça");

        jLabel34.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel34.setText("Classe");

        jLabel35.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel35.setText("Sexo");

        SexoBox.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        SexoBox.setModel(new javax.swing.DefaultComboBoxModel(EnumSexo.values()));

        jLabel36.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel36.setText("Idade");
        jLabel36.setToolTipText("Idade");

        jLabel38.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel38.setText("Deslocamento");

        lblDesc.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblDesc.setText("0");

        jLabel40.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel40.setText("Divindade");

        jLabel41.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel41.setText("Tamanho");

        TamanhoBox.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        TamanhoBox.setModel(new javax.swing.DefaultComboBoxModel(EnumTamanho.values()));
        TamanhoBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TamanhoBoxActionPerformed(evt);
            }
        });

        jComboBox1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Allihanna", "Azgher", "Grande Oceano", "Hyninn", "Keenn", "Khalmyr", "Lena", "Lin-Wu", "Marah", "Megalokk", "Nimb", "Ragnar", "Sszzaas", "Tanna-Toh", "Tauron", "Tenebra", "Thyatis", "Valkaria", "Wynna" }));

        Logotipo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagens/tormenta-rpg2.png"))); // NOI18N

        ButtonRolarFORT.setText("Rolar");
        ButtonRolarFORT.setToolTipText("1d20 + lv/2 + ModCon");
        ButtonRolarFORT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonRolarFORTActionPerformed(evt);
            }
        });

        lblTesteFORT.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblTesteFORT.setText("0");
        lblTesteFORT.setToolTipText("1d20 + lv/2 + ModCon");

        jLabel32.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel32.setText("Teste Resistencia");

        lblIdade.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblIdade.setText("00");

        ButtonIdade.setText("Rolar");
        ButtonIdade.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonIdadeActionPerformed(evt);
            }
        });

        spinnerFOR.setModel(new javax.swing.SpinnerNumberModel(10, 0, null, 1));
        spinnerFOR.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spinnerFORStateChanged(evt);
            }
        });

        spinnerDES.setModel(new javax.swing.SpinnerNumberModel(10, 0, null, 1));
        spinnerDES.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spinnerDESStateChanged(evt);
            }
        });

        spinnerCON.setModel(new javax.swing.SpinnerNumberModel(10, 0, null, 1));
        spinnerCON.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spinnerCONStateChanged(evt);
            }
        });

        spinnerINT.setModel(new javax.swing.SpinnerNumberModel(10, 0, null, 1));
        spinnerINT.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spinnerINTStateChanged(evt);
            }
        });

        spinnerSAB.setModel(new javax.swing.SpinnerNumberModel(10, 0, null, 1));
        spinnerSAB.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spinnerSABStateChanged(evt);
            }
        });

        spinnerCAR.setModel(new javax.swing.SpinnerNumberModel(10, 0, null, 1));
        spinnerCAR.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spinnerCARStateChanged(evt);
            }
        });

        jCheckBoxAcrobacia.setText("Acrobacia");
        jCheckBoxAcrobacia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxAcrobaciaActionPerformed(evt);
            }
        });

        jCheckBoxAdestrar.setText("Adestrar Animais");

        jCheckBoxAtletismo.setText("Atletismo");

        jCheckBoxAtuaca.setText("Atuação");

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setText("Perícias");

        jLabel4.setText("Graduação");

        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("0");

        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel10.setText("0");

        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel11.setText("0");

        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel12.setText("0");

        jLabel14.setText("Corpor a corpo");

        jLabel16.setText("A distancia");

        lblCac.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblCac.setText("0");

        lblAdist.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblAdist.setText("0");

        lblCaCbba.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblCaCbba.setText("0");

        lblCaCmodFOR.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblCaCmodFOR.setText("0");

        lblCaCTamanho.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblCaCTamanho.setText("0");

        lblDistBba.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblDistBba.setText("0");

        lblDistModDes.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblDistModDes.setText("0");

        lblDistModTam.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblDistModTam.setText("0");

        jLabel13.setText("BBA");

        jLabel15.setText("ModCon");

        jLabel17.setText("Tam");

        jLabel18.setText("Total");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel31)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(RaçaBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel35)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(SexoBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel36)
                            .addComponent(jLabel34))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(ClasseBox, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(29, 29, 29))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblIdade, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(18, 18, 18)
                                .addComponent(ButtonIdade)
                                .addGap(18, 18, 18)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel33)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(NivelSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel38)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblDesc))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel40)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addComponent(jLabel9)
                                        .addGap(10, 10, 10)
                                        .addComponent(spinnerCAR, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(jLabel8)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(spinnerSAB, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(jLabel7)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(spinnerINT, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                            .addComponent(LblFOR)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 11, Short.MAX_VALUE)
                                            .addComponent(spinnerFOR, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(layout.createSequentialGroup()
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(lblDES)
                                                .addComponent(jLabel6))
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addComponent(spinnerCON, javax.swing.GroupLayout.DEFAULT_SIZE, 45, Short.MAX_VALUE)
                                                .addComponent(spinnerDES))))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel16)
                                        .addComponent(jLabel14)))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(156, 156, 156)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(lblAdist)
                                            .addComponent(lblCac)))
                                    .addGroup(layout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(lblModDes, javax.swing.GroupLayout.Alignment.TRAILING)
                                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(lblModFor)
                                                    .addComponent(lblModCar, javax.swing.GroupLayout.Alignment.TRAILING)))
                                            .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addGroup(layout.createSequentialGroup()
                                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                            .addComponent(lblModSab)
                                                            .addComponent(lblModInt)
                                                            .addComponent(lblModCon)
                                                            .addGroup(layout.createSequentialGroup()
                                                                .addGap(18, 18, 18)
                                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                    .addComponent(lblDistBba)
                                                                    .addComponent(lblCaCbba))))
                                                        .addGap(17, 17, 17))
                                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                                        .addComponent(jLabel13)
                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)))
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(jLabel24)
                                                    .addGroup(layout.createSequentialGroup()
                                                        .addGap(2, 2, 2)
                                                        .addComponent(jLabel23))
                                                    .addGroup(layout.createSequentialGroup()
                                                        .addGap(2, 2, 2)
                                                        .addComponent(jLabel25))
                                                    .addGroup(layout.createSequentialGroup()
                                                        .addGap(4, 4, 4)
                                                        .addComponent(jLabel22))
                                                    .addGroup(layout.createSequentialGroup()
                                                        .addGap(4, 4, 4)
                                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                            .addComponent(jLabel26)
                                                            .addGroup(layout.createSequentialGroup()
                                                                .addGap(16, 16, 16)
                                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                                    .addComponent(lblDistModDes)
                                                                    .addComponent(lblCaCmodFOR))
                                                                .addGap(35, 35, 35)
                                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                    .addComponent(lblCaCTamanho)
                                                                    .addComponent(lblDistModTam)))
                                                            .addGroup(layout.createSequentialGroup()
                                                                .addComponent(jLabel15)
                                                                .addGap(14, 14, 14)
                                                                .addComponent(jLabel17)))))
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addGroup(layout.createSequentialGroup()
                                                        .addGap(40, 40, 40)
                                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                            .addComponent(lblFORT, javax.swing.GroupLayout.Alignment.TRAILING)
                                                            .addComponent(lblREF, javax.swing.GroupLayout.Alignment.TRAILING)
                                                            .addComponent(lblVON, javax.swing.GroupLayout.Alignment.TRAILING)
                                                            .addComponent(lblCA, javax.swing.GroupLayout.Alignment.TRAILING)
                                                            .addComponent(lblPV, javax.swing.GroupLayout.Alignment.TRAILING)))
                                                    .addGroup(layout.createSequentialGroup()
                                                        .addGap(18, 18, 18)
                                                        .addComponent(jLabel18)))))
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(layout.createSequentialGroup()
                                                .addGap(35, 35, 35)
                                                .addComponent(jLabel32))
                                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(ButtonRolarFORT)
                                                .addGap(30, 30, 30)
                                                .addComponent(lblTesteFORT, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(17, 17, 17))))))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(NomePersonagem, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(12, 12, 12)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(NomeJogador, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel3)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel41)
                                .addGap(20, 20, 20)
                                .addComponent(TamanhoBox, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(Logotipo, javax.swing.GroupLayout.PREFERRED_SIZE, 206, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(18, 18, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addComponent(jLabel4)
                                        .addGap(19, 19, 19))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(jCheckBoxAcrobacia)
                                                .addGap(67, 67, 67))
                                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(jCheckBoxAdestrar)
                                                    .addComponent(jCheckBoxAtletismo)
                                                    .addComponent(jCheckBoxAtuaca)
                                                    .addComponent(jLabel1))
                                                .addGap(33, 33, 33)))
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addComponent(NextPage, javax.swing.GroupLayout.Alignment.TRAILING))
                                .addGap(17, 17, 17))))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(23, 23, 23)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(NomePersonagem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(NomeJogador, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel31)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(RaçaBox, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel34, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(ClasseBox, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel33)
                                        .addComponent(NivelSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel38, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(lblDesc)))
                                .addGap(34, 34, 34)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel35)
                                        .addComponent(SexoBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel40)
                                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(ButtonIdade))))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel36)
                                .addComponent(lblIdade))))
                    .addComponent(Logotipo))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(ButtonRolarFORT)
                                    .addComponent(lblTesteFORT, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(120, 120, 120))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(18, 18, 18)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(jLabel41)
                                            .addComponent(TamanhoBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                    .addComponent(LblFOR)
                                                    .addComponent(spinnerFOR, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGap(18, 18, 18)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                    .addComponent(lblDES)
                                                    .addComponent(spinnerDES, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGap(11, 11, 11)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                    .addComponent(jLabel6)
                                                    .addComponent(spinnerCON, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGap(18, 18, 18)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                    .addComponent(jLabel7)
                                                    .addComponent(spinnerINT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGap(9, 9, 9)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                    .addComponent(jLabel8)
                                                    .addComponent(spinnerSAB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGap(15, 15, 15)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                    .addComponent(jLabel9)
                                                    .addComponent(spinnerCAR, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                            .addGroup(layout.createSequentialGroup()
                                                .addGap(72, 72, 72)
                                                .addComponent(lblModCon)
                                                .addGap(18, 18, 18)
                                                .addComponent(lblModInt)
                                                .addGap(18, 18, 18)
                                                .addComponent(lblModSab)
                                                .addGap(18, 18, 18)
                                                .addComponent(lblModCar))
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(lblModFor)
                                                .addGap(18, 18, 18)
                                                .addComponent(lblModDes))))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(48, 48, 48)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(jLabel22)
                                                .addGap(18, 18, 18)
                                                .addComponent(jLabel23))
                                            .addGroup(layout.createSequentialGroup()
                                                .addGap(5, 5, 5)
                                                .addComponent(lblPV)
                                                .addGap(15, 15, 15)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                    .addComponent(lblCA)
                                                    .addComponent(jLabel32))))
                                        .addGap(29, 29, 29)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(jLabel24)
                                            .addComponent(lblFORT))
                                        .addGap(18, 18, 18)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(jLabel25)
                                            .addComponent(lblREF))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(jLabel26)
                                            .addComponent(lblVON))))
                                .addGap(18, 18, 18)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel13)
                            .addComponent(jLabel15)
                            .addComponent(jLabel17)
                            .addComponent(jLabel18))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel14)
                            .addComponent(lblCac)
                            .addComponent(lblCaCbba)
                            .addComponent(lblCaCmodFOR)
                            .addComponent(lblCaCTamanho))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblAdist)
                            .addComponent(lblDistBba)
                            .addComponent(lblDistModDes)
                            .addComponent(lblDistModTam))
                        .addGap(7, 7, 7))
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel1)
                        .addGap(2, 2, 2)
                        .addComponent(jLabel4)
                        .addGap(20, 20, 20)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jCheckBoxAcrobacia)
                            .addComponent(jLabel5))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jCheckBoxAdestrar)
                            .addComponent(jLabel10))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jCheckBoxAtletismo)
                            .addComponent(jLabel11))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jCheckBoxAtuaca)
                            .addComponent(jLabel12))
                        .addGap(26, 26, 26)
                        .addComponent(NextPage)
                        .addGap(20, 20, 20))))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void NextPageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NextPageActionPerformed
        // TODO add your handling code here:
        NextView next = new NextView(personagem);
        next.setTitle("Ficha RPG Tormenta");
        next.setSize(674, 700);
        next.setVisible(true);
        dispose();
    }//GEN-LAST:event_NextPageActionPerformed

    private void RaçaBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RaçaBoxActionPerformed
        switch (RaçaBox.getSelectedIndex()) {
            case 0:
                opRaca_Anterior = 0;
                personagem.zerar();
                racaOK = 0;
                break;
            case 1:
                setarRacas();
                opRaca_Anterior = 1;
                modRaca = 4;
                anão.novaRaça(personagem, resistencia);
                TamanhoBox.setSelectedIndex(5);
                racaOK = 1;
                break;
            case 2:
                setarRacas();
                opRaca_Anterior = 2;
                modRaca = 4;
                elfo.novaRaça(personagem, resistencia);
                TamanhoBox.setSelectedIndex(5);
                racaOK = 1;
                break;
            case 3:
                setarRacas();
                opRaca_Anterior = 3;
                modRaca = 4;
                goblin.novaRaça(personagem, resistencia);
                TamanhoBox.setSelectedIndex(4);
                racaOK = 1;
                break;
            case 4:
                setarRacas();
                opRaca_Anterior = 4;
                modRaca = 4;
                halfling.novaRaça(personagem, resistencia);
                TamanhoBox.setSelectedIndex(4);
                racaOK = 1;
                break;
            case 5:
                setarRacas();
                opRaca_Anterior = 5;
                modRaca = 4;
                humano.ChamarTela(pericias, personagem, resistencia, this, RaçaBox.getSelectedIndex());
                TamanhoBox.setSelectedIndex(5);
                racaOK = 1;
                break;
            case 6:
                setarRacas();
                opRaca_Anterior = 6;
                modRaca = 4;
                lefou.ChamarTela(pericias, personagem, resistencia, this, RaçaBox.getSelectedIndex());
                TamanhoBox.setSelectedIndex(5);
                racaOK = 1;
                break;
            case 7:
                setarRacas();
                opRaca_Anterior = 7;
                modRaca = 4;
                minotauro.novaRaça(personagem, resistencia);
                TamanhoBox.setSelectedIndex(5);
                racaOK = 1;
                break;
            case 8:
                setarRacas();
                opRaca_Anterior = 8;
                modRaca = 4;
                qareen.novaRaça(personagem, resistencia);
                TamanhoBox.setSelectedIndex(5);
                racaOK = 1;
                break;
        }
        setTotal(15);
        refreshView(personagem);
    }//GEN-LAST:event_RaçaBoxActionPerformed

    private void ClasseBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ClasseBoxActionPerformed

        switch (ClasseBox.getSelectedIndex()) {
            case 1:
                barbaro.criarNovaClasse(personagem, pericias, resistencia, talentosC, idade);
                break;
            case 2:
                bardo.criarNovaClasse(personagem, pericias, resistencia, talentosC, idade);
                break;
            case 3:
                clerigo.criarNovaClasse(personagem, pericias, resistencia, talentosC, idade);
                break;
            case 4:
                druida.criarNovaClasse(personagem, pericias, resistencia, talentosC, idade);
                break;
            case 5:
                feiticeiro.criarNovaClasse(personagem, pericias, resistencia, talentosC, idade);
                break;
            case 6:
                guerreiro.criarNovaClasse(personagem, pericias, resistencia, talentosC, idade);
                break;
            case 7:
                ladino.criarNovaClasse(personagem, pericias, resistencia, talentosC, idade);
                break;
            case 8:
                mago.criarNovaClasse(personagem, pericias, resistencia, talentosC, idade);
                break;
            case 9:
                monge.criarNovaClasse(personagem, pericias, resistencia, talentosC, idade);
                break;
            case 10:
                paladino.criarNovaClasse(personagem, pericias, resistencia, talentosC, idade);
                break;
            case 11:
                ranger.criarNovaClasse(personagem, pericias, resistencia, talentosC, idade);
                break;
            case 12:
                samurai.criarNovaClasse(personagem, pericias, resistencia, talentosC, idade);
                break;
            case 13:
                swashbuckler.criarNovaClasse(personagem, pericias, resistencia, talentosC, idade);
                break;
        }
        refreshView(personagem);
    }//GEN-LAST:event_ClasseBoxActionPerformed

    private void NomePersonagemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NomePersonagemActionPerformed

    }//GEN-LAST:event_NomePersonagemActionPerformed

    private void lblModForKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_lblModForKeyReleased

    }//GEN-LAST:event_lblModForKeyReleased

    private void lblModForInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_lblModForInputMethodTextChanged

    }//GEN-LAST:event_lblModForInputMethodTextChanged

    private void NivelSpinnerInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_NivelSpinnerInputMethodTextChanged

    }//GEN-LAST:event_NivelSpinnerInputMethodTextChanged

    private void NivelSpinnerStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_NivelSpinnerStateChanged
        personagem.setNivel((Integer) (NivelSpinner.getValue()));
        personagem.totalClasseArmadura();
        addTotal((personagem.getNivel() / 2));
        System.out.println("NIVEL:" + personagem.getNivel());
        /*switch (ClasseBox.getSelectedIndex()) {
            case 1:
                barbaro.setBBA(personagem);
                resistencia.setFORT(personagem, 0);
                break;
            case 2:
                bardo.setBBA(personagem);
                resistencia.setFORT(personagem, 0);
                break;
            case 3:
                clerigo.setBBA(personagem);
                resistencia.setFORT(personagem, 0);
                break;
            case 4:
                druida.setBBA(personagem);
                resistencia.setFORT(personagem, 0);
                break;
            case 5:
                feiticeiro.setBBA(personagem);
                resistencia.setFORT(personagem, 0);
                break;
            case 6:
                guerreiro.setBBA(personagem);
                resistencia.setFORT(personagem, 0);
                break;
            case 7:
                ladino.setBBA(personagem);
                resistencia.setFORT(personagem, 0);
                break;
            case 8:
                mago.setBBA(personagem);
                resistencia.setFORT(personagem, 0);
                break;
            case 9:
                monge.setBBA(personagem);
                resistencia.setFORT(personagem, 0);
                break;
            case 10:
                paladino.setBBA(personagem);
                resistencia.setFORT(personagem, 0);
                break;
            case 11:
                ranger.setBBA(personagem);
                resistencia.setFORT(personagem, 0);
                break;
            case 12:
                samurai.setBBA(personagem);
                resistencia.setFORT(personagem, 2);
                break;
            case 13:
                swashbuckler.setBBA(personagem);
                resistencia.setFORT(personagem, 0);
                break;
        }*/
 /*if(classe instanceof Bardo){
            ((Bardo) classe).setBBA(personagem);
        }else if(classe instanceof Bárbaro){
            ((Bárbaro) classe).setBBA(personagem);
        }else if(classe instanceof Clerigo){
            ((Clerigo) classe).setBBA(personagem);
        }else if(classe instanceof Druida){
            ((Druida) classe).setBBA(personagem);
        }else if(classe instanceof Feiticeiro){
            ((Feiticeiro) classe).setBBA(personagem);
        }else if(classe instanceof Guerreiro){
            ((Guerreiro) classe).setBBA(personagem);
        }else if(classe instanceof Ladino){
            ((Ladino) classe).setBBA(personagem);
        }else if(classe instanceof Mago){
            ((Mago) classe).setBBA(personagem);
        }else if(classe instanceof Monge){
            ((Monge) classe).setBBA(personagem);
        }else if(classe instanceof Paladino){
            ((Paladino) classe).setBBA(personagem);
        }else if(classe instanceof Ranger){
            ((Ranger) classe).setBBA(personagem);
        }else if(classe instanceof Samurai){
            ((Samurai) classe).setBBA(personagem);
        }else if(classe instanceof Swashbuckler){
            ((Swashbuckler) classe).setBBA(personagem);
        }*/
        refreshView(personagem);
    }//GEN-LAST:event_NivelSpinnerStateChanged

    private void ButtonRolarFORTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonRolarFORTActionPerformed
        lblTesteFORT.setText(Integer.toString(resistencia.TesteResistencia(resistencia.getFORT(), dado.d20())));
    }//GEN-LAST:event_ButtonRolarFORTActionPerformed

    private void ButtonIdadeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonIdadeActionPerformed
        lblIdade.setText(Integer.toString(personagem.AgoraIdade(idade, personagem.getIdadeRaca(), personagem.getIdadeClasse())));
    }//GEN-LAST:event_ButtonIdadeActionPerformed

    private void RaçaBoxMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_RaçaBoxMouseClicked
        refreshView(personagem);
    }//GEN-LAST:event_RaçaBoxMouseClicked

    private void TamanhoBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TamanhoBoxActionPerformed

        switch (RaçaBox.getSelectedIndex()) {
            case 1:
                if (TamanhoBox.getSelectedIndex() != 5) {
                    JOptionPane.showMessageDialog(null, "Só pode ser tamanho Médio!");
                    TamanhoBox.setSelectedIndex(5);
                }
                break;
            case 2:
                if (TamanhoBox.getSelectedIndex() != 5) {
                    JOptionPane.showMessageDialog(null, "Só pode ser tamanho Médio!");
                    TamanhoBox.setSelectedIndex(5);
                }
                break;
            case 3:
                if (TamanhoBox.getSelectedIndex() != 4) {
                    JOptionPane.showMessageDialog(null, "Só pode ser tamanho Pequeno!");
                    TamanhoBox.setSelectedIndex(4);
                }
                break;
            case 4:
                if (TamanhoBox.getSelectedIndex() != 4) {
                    JOptionPane.showMessageDialog(null, "Só pode ser tamanho Pequeno!");
                    TamanhoBox.setSelectedIndex(4);
                }
                break;
            case 5:
                if (TamanhoBox.getSelectedIndex() != 5) {
                    JOptionPane.showMessageDialog(null, "Só pode ser tamanho Médio!");
                    TamanhoBox.setSelectedIndex(5);
                }
                break;
            case 6:
                break;
            case 7:
                if (TamanhoBox.getSelectedIndex() != 5) {
                    JOptionPane.showMessageDialog(null, "Só pode ser tamanho Médio!");
                    TamanhoBox.setSelectedIndex(5);
                }
                break;
            case 8:
                if (TamanhoBox.getSelectedIndex() != 5) {
                    JOptionPane.showMessageDialog(null, "Só pode ser tamanho Médio!");
                    TamanhoBox.setSelectedIndex(5);
                }
                break;
        }

        personagem.setModTamanho(TamanhoBox.getSelectedIndex());

        refreshView(personagem);
    }//GEN-LAST:event_TamanhoBoxActionPerformed

    private void spinnerFORStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spinnerFORStateChanged
        //lblModFor.setText(Integer.toString(personagem.getModFOR()));
        int valorFOR = ((int) spinnerFOR.getValue());

        if (valorFOR > valoranteriorFOR) {
            Total--;
        } else if (valorFOR < valoranteriorFOR) {
            Total++;
        }
        valoranteriorFOR = valorFOR;
        if (getTotal() != -1) {
            personagem.setFOR((Integer) (spinnerFOR.getValue()));
            personagem.setModFOR();
        }
        refreshView(personagem);
    }//GEN-LAST:event_spinnerFORStateChanged

    private void spinnerDESStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spinnerDESStateChanged

        int valorDES = ((int) spinnerDES.getValue());

        if (valorDES > valoranteriorDES) {
            Total--;
        } else if (valorDES < valoranteriorDES) {
            Total++;
        }
        valoranteriorDES = valorDES;
        if (getTotal() != -1) {
            personagem.setDES((Integer) (spinnerDES.getValue()));
            personagem.setModDES();
        }
        //lblModDes.setText(Integer.toString(personagem.getModDES()));
        refreshView(personagem);
        //resistencia.setREF(personagem);
        //lblREF.setText(Integer.toString(resistencia.getREF()));

    }//GEN-LAST:event_spinnerDESStateChanged

    private void spinnerCONStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spinnerCONStateChanged

        int valorCON = ((int) spinnerCON.getValue());

        if (valorCON > valoranteriorCON) {
            Total--;
        } else if (valorCON < valoranteriorCON) {
            Total++;
        }
        valoranteriorCON = valorCON;

        if (getTotal() != -1) {
            personagem.setCON((Integer) (spinnerCON.getValue()));
            personagem.setModCON();
        }
        refreshView(personagem);
        //resistencia.setFORT(personagem, 0);
        //lblFORT.setText(Integer.toString(resistencia.getFORT()));
    }//GEN-LAST:event_spinnerCONStateChanged

    private void spinnerINTStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spinnerINTStateChanged

        int valorINT = ((int) spinnerINT.getValue());

        if (valorINT > valoranteriorINT) {
            Total--;
        } else if (valorINT < valoranteriorINT) {
            Total++;
        }
        valoranteriorINT = valorINT;

        if (getTotal() != -1) {
            personagem.setINT((Integer) (spinnerINT.getValue()));
            personagem.setModINT();
        }
        refreshView(personagem);
        //lblModInt.setText(Integer.toString(personagem.getModINT()));
    }//GEN-LAST:event_spinnerINTStateChanged

    private void spinnerSABStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spinnerSABStateChanged

        int valorSAB = ((int) spinnerSAB.getValue());
        if (valorSAB > valoranteriorSAB) {
            Total--;
        } else if (valorSAB < valoranteriorSAB) {
            Total++;
        }
        valoranteriorSAB = valorSAB;

        if (getTotal() != -1) {
            personagem.setSAB((Integer) (spinnerSAB.getValue()));
            personagem.setModSAB();
        }
        refreshView(personagem);
        //resistencia.setVON(personagem);
        //lblVON.setText(Integer.toString(resistencia.getVON()));
    }//GEN-LAST:event_spinnerSABStateChanged

    private void spinnerCARStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spinnerCARStateChanged

        int valorCAR = ((int) spinnerCAR.getValue());

        if (valorCAR > valoranteriorCAR) {
            Total--;
        } else if (valorCAR < valoranteriorCAR) {
            Total++;
        }

        valoranteriorCAR = valorCAR;

        if (getTotal() != -1) {
            personagem.setCAR((Integer) (spinnerCAR.getValue()));
            personagem.setModCAR();
        }
        refreshView(personagem);
        //lblModCar.setText(Integer.toString(personagem.getModCAR()));
    }//GEN-LAST:event_spinnerCARStateChanged

    private void jCheckBoxAcrobaciaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxAcrobaciaActionPerformed
        if (jCheckBoxAcrobacia.isSelected()) {

        }
    }//GEN-LAST:event_jCheckBoxAcrobaciaActionPerformed

    public void somaHabilidades() {
        if (personagem.getNivel() == 1 && racaOK == 0) { //Primeiro nível sem raça
            if ((int) spinnerFOR.getValue() > 20) {
                spinnerFOR.setValue(spinnerFOR.getPreviousValue());
            } else if ((int) spinnerFOR.getValue() < 7) {
                spinnerFOR.setValue(spinnerFOR.getNextValue());
            } else if ((int) spinnerDES.getValue() > 20) {
                spinnerDES.setValue(spinnerDES.getPreviousValue());
            } else if ((int) spinnerDES.getValue() < 7) {
                spinnerDES.setValue(spinnerDES.getNextValue());
            } else if ((int) spinnerCON.getValue() > 20) {
                spinnerCON.setValue(spinnerCON.getPreviousValue());
            } else if ((int) spinnerCON.getValue() < 7) {
                spinnerCON.setValue(spinnerCON.getNextValue());
            } else if ((int) spinnerINT.getValue() > 20) {
                spinnerINT.setValue(spinnerINT.getPreviousValue());
            } else if ((int) spinnerINT.getValue() < 7) {
                spinnerINT.setValue(spinnerINT.getNextValue());
            } else if ((int) spinnerSAB.getValue() > 20) {
                spinnerSAB.setValue(spinnerSAB.getPreviousValue());
            } else if ((int) spinnerSAB.getValue() < 7) {
                spinnerSAB.setValue(spinnerSAB.getNextValue());
            } else if ((int) spinnerCAR.getValue() > 20) {
                spinnerCAR.setValue(spinnerCAR.getPreviousValue());
            } else if ((int) spinnerCAR.getValue() < 7) {
                spinnerCAR.setValue(spinnerCAR.getNextValue());
            }
            if (getTotal() == (0)) {
                NextPage.setVisible(true);
            }
        } else if (personagem.getNivel() == 1 && racaOK == 1) { //Primeiro nível com raça
            if (((int) spinnerFOR.getValue() > 22)) {
                spinnerFOR.setValue(spinnerFOR.getPreviousValue());
            } else if ((int) spinnerFOR.getValue() < 7) {
                spinnerFOR.setValue(spinnerFOR.getNextValue());
            } else if (((int) spinnerDES.getValue() > 22)) {
                spinnerDES.setValue(spinnerDES.getPreviousValue());
            } else if ((int) spinnerDES.getValue() < 7) {
                spinnerDES.setValue(spinnerDES.getNextValue());
            } else if (((int) spinnerCON.getValue() > 22)) {
                spinnerCON.setValue(spinnerCON.getPreviousValue());
            } else if ((int) spinnerCON.getValue() < 7) {
                spinnerCON.setValue(spinnerCON.getNextValue());
            } else if (((int) spinnerINT.getValue() > 22)) {
                spinnerINT.setValue(spinnerINT.getPreviousValue());
            } else if ((int) spinnerINT.getValue() < 7) {
                spinnerINT.setValue(spinnerINT.getNextValue());
            } else if (((int) spinnerSAB.getValue() > 22)) {
                spinnerSAB.setValue(spinnerSAB.getPreviousValue());
            } else if ((int) spinnerSAB.getValue() < 7) {
                spinnerSAB.setValue(spinnerSAB.getNextValue());
            } else if (((int) spinnerCAR.getValue() > 22)) {
                spinnerCAR.setValue(spinnerCAR.getPreviousValue());
            } else if ((int) spinnerCAR.getValue() < 7) {
                spinnerCAR.setValue(spinnerCAR.getNextValue());
            }
            if (getTotal() == 0) {
                NextPage.setVisible(true);
            } else if (getTotal() == (0)) {
                NextPage.setVisible(true);
            }
        } else if (personagem.getNivel() > 1 && racaOK == 1) {
            if ((int) spinnerFOR.getValue() < 7) {
                spinnerFOR.setValue(spinnerFOR.getNextValue());
            } else if ((int) spinnerDES.getValue() < 7) {
                spinnerDES.setValue(spinnerDES.getNextValue());
            } else if ((int) spinnerCON.getValue() < 7) {
                spinnerCON.setValue(spinnerCON.getNextValue());
            } else if ((int) spinnerINT.getValue() < 7) {
                spinnerINT.setValue(spinnerINT.getNextValue());
            } else if ((int) spinnerSAB.getValue() < 7) {
                spinnerSAB.setValue(spinnerSAB.getNextValue());
            } else if ((int) spinnerCAR.getValue() < 7) {
                spinnerCAR.setValue(spinnerCAR.getNextValue());
            }
            if (getTotal() == 0) {
                NextPage.setVisible(true);
            } else if (getTotal() == (0)) {
                NextPage.setVisible(true);
            }
        } else if (getTotal() == (0)) {
            NextPage.setVisible(true);
            if (getTotal() > 0) {
                NextPage.setVisible(false);
            }
        } else {
            NextPage.setVisible(false);
        }
        System.out.println("TOTAL:" + getTotal());
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */

        //lblPV.setText(personagem.getVida);
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FichaRPG.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FichaRPG.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FichaRPG.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FichaRPG.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                FichaRPG tela = new FichaRPG();
                tela.setTitle("Ficha RPG Tormenta");
                tela.setSize(720, 620);
                tela.setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ButtonIdade;
    private javax.swing.JButton ButtonRolarFORT;
    private javax.swing.JComboBox<String> ClasseBox;
    private javax.swing.JLabel LblFOR;
    private javax.swing.JLabel Logotipo;
    private javax.swing.JButton NextPage;
    private javax.swing.JSpinner NivelSpinner;
    private javax.swing.JTextField NomeJogador;
    private javax.swing.JTextField NomePersonagem;
    private javax.swing.JComboBox<String> RaçaBox;
    private javax.swing.JComboBox<String> SexoBox;
    private javax.swing.JComboBox<String> TamanhoBox;
    private javax.swing.JCheckBox jCheckBoxAcrobacia;
    private javax.swing.JCheckBox jCheckBoxAdestrar;
    private javax.swing.JCheckBox jCheckBoxAtletismo;
    private javax.swing.JCheckBox jCheckBoxAtuaca;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel lblAdist;
    private javax.swing.JLabel lblCA;
    private javax.swing.JLabel lblCaCTamanho;
    private javax.swing.JLabel lblCaCbba;
    private javax.swing.JLabel lblCaCmodFOR;
    private javax.swing.JLabel lblCac;
    private javax.swing.JLabel lblDES;
    private javax.swing.JLabel lblDesc;
    private javax.swing.JLabel lblDistBba;
    private javax.swing.JLabel lblDistModDes;
    private javax.swing.JLabel lblDistModTam;
    private javax.swing.JLabel lblFORT;
    private javax.swing.JLabel lblIdade;
    private javax.swing.JLabel lblModCar;
    private javax.swing.JLabel lblModCon;
    private javax.swing.JLabel lblModDes;
    private javax.swing.JLabel lblModFor;
    private javax.swing.JLabel lblModInt;
    private javax.swing.JLabel lblModSab;
    private javax.swing.JLabel lblPV;
    private javax.swing.JLabel lblREF;
    private javax.swing.JLabel lblTesteFORT;
    private javax.swing.JLabel lblVON;
    private javax.swing.JSpinner spinnerCAR;
    private javax.swing.JSpinner spinnerCON;
    private javax.swing.JSpinner spinnerDES;
    private javax.swing.JSpinner spinnerFOR;
    private javax.swing.JSpinner spinnerINT;
    private javax.swing.JSpinner spinnerSAB;
    // End of variables declaration//GEN-END:variables
}
