/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Ficha.Personagem;
import java.util.ArrayList;
import Ficha.Perícias;

/**
 *
 * @author lincoln
 */
public class NextView extends javax.swing.JFrame {

    /**
     * Creates new form NextView
     */
    Personagem personagem;

    public NextView() {
        initComponents();

    }

    public NextView(Personagem p) {
        initComponents();
        this.personagem = p;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btmBack = new javax.swing.JButton();
        CheckAcrobacia = new javax.swing.JCheckBox();
        CheckAdestrar = new javax.swing.JCheckBox();
        CheckAtletismo = new javax.swing.JCheckBox();
        CheckAtuação = new javax.swing.JCheckBox();
        CheckCavalgar = new javax.swing.JCheckBox();
        CheckConhecimento = new javax.swing.JCheckBox();
        CheckCura = new javax.swing.JCheckBox();
        CheckDiplomacia = new javax.swing.JCheckBox();
        CheckEnganação = new javax.swing.JCheckBox();
        CheckFurtividade = new javax.swing.JCheckBox();
        CheckIdentificar = new javax.swing.JCheckBox();
        CheckIniciativa = new javax.swing.JCheckBox();
        CheckIntimidação = new javax.swing.JCheckBox();
        CheckIntuição = new javax.swing.JCheckBox();
        CheckLadinagem = new javax.swing.JCheckBox();
        CheckObterInformação = new javax.swing.JCheckBox();
        CheckOfício = new javax.swing.JCheckBox();
        CheckPercepção = new javax.swing.JCheckBox();
        CheckSobrevivência = new javax.swing.JCheckBox();
        jLabel1 = new javax.swing.JLabel();
        lblAcrobacia = new javax.swing.JLabel();
        lblAdestrar = new javax.swing.JLabel();
        lblAtletismo = new javax.swing.JLabel();
        lblAtuação = new javax.swing.JLabel();
        lblCavalgar = new javax.swing.JLabel();
        lblConhecimento = new javax.swing.JLabel();
        lblCura = new javax.swing.JLabel();
        lblDiplomacia = new javax.swing.JLabel();
        lblEnganação = new javax.swing.JLabel();
        lblFurtividade = new javax.swing.JLabel();
        lblIdentificarMagia = new javax.swing.JLabel();
        lblIniciativa = new javax.swing.JLabel();
        lblIntimidação = new javax.swing.JLabel();
        lblIntuição = new javax.swing.JLabel();
        lblLadinagem = new javax.swing.JLabel();
        lblObterInformação = new javax.swing.JLabel();
        lblOfício = new javax.swing.JLabel();
        lblPercepção = new javax.swing.JLabel();
        lblSobrevivência = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        btmBack.setText("Back");
        btmBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btmBackActionPerformed(evt);
            }
        });

        CheckAcrobacia.setText("Acrobacia");

        CheckAdestrar.setText("Adestrar animais");

        CheckAtletismo.setText("Atletismo");

        CheckAtuação.setText("Atuação");

        CheckCavalgar.setText("Cavalgar");

        CheckConhecimento.setText("Conhecimento");

        CheckCura.setText("Cura");

        CheckDiplomacia.setText("Diplomacia");

        CheckEnganação.setText("Enganação");

        CheckFurtividade.setText("Furtividade");

        CheckIdentificar.setText("Identificar magia");

        CheckIniciativa.setText("Iniciativa");

        CheckIntimidação.setText("Intimidação");

        CheckIntuição.setText("Intuição");

        CheckLadinagem.setText("Ladinagem ");

        CheckObterInformação.setText("Obter informação");

        CheckOfício.setText("Ofício");

        CheckPercepção.setText("Percepção");

        CheckSobrevivência.setText("Sobrevivência");

        jLabel1.setText("Perícias");

        lblAcrobacia.setText("0");

        lblAdestrar.setText("0");

        lblAtletismo.setText("0");

        lblAtuação.setText("0");

        lblCavalgar.setText("0");

        lblConhecimento.setText("0");

        lblCura.setText("0");

        lblDiplomacia.setText("0");

        lblEnganação.setText("0");

        lblFurtividade.setText("0");

        lblIdentificarMagia.setText("0");

        lblIniciativa.setText("0");

        lblIntimidação.setText("0");

        lblIntuição.setText("0");

        lblLadinagem.setText("0");

        lblObterInformação.setText("0");

        lblOfício.setText("0");

        lblPercepção.setText("0");

        lblSobrevivência.setText("0");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(CheckAcrobacia)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblAcrobacia))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btmBack)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(CheckAdestrar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblAdestrar))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(CheckAtletismo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblAtletismo))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(CheckAtuação)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblAtuação))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(CheckCavalgar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblCavalgar))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(CheckConhecimento)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblConhecimento))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(CheckCura)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblCura))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(CheckDiplomacia)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblDiplomacia))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(CheckEnganação)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblEnganação))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(CheckFurtividade)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblFurtividade))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(CheckIdentificar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblIdentificarMagia))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(CheckIniciativa)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblIniciativa))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(CheckIntimidação)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblIntimidação))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(CheckIntuição)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblIntuição))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(CheckLadinagem)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblLadinagem))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(CheckObterInformação)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblObterInformação))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(CheckOfício)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblOfício))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(CheckPercepção)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblPercepção))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(CheckSobrevivência)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblSobrevivência)))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 212, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(190, 190, 190))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jLabel1)
                .addGap(23, 23, 23)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CheckAcrobacia)
                    .addComponent(lblAcrobacia))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CheckAdestrar)
                    .addComponent(lblAdestrar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CheckAtletismo)
                    .addComponent(lblAtletismo))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CheckAtuação)
                    .addComponent(lblAtuação))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CheckCavalgar)
                    .addComponent(lblCavalgar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CheckConhecimento)
                    .addComponent(lblConhecimento))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CheckCura)
                    .addComponent(lblCura))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CheckDiplomacia)
                    .addComponent(lblDiplomacia))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CheckEnganação)
                    .addComponent(lblEnganação))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CheckFurtividade)
                    .addComponent(lblFurtividade))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CheckIdentificar)
                    .addComponent(lblIdentificarMagia))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CheckIniciativa)
                    .addComponent(lblIniciativa))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CheckIntimidação)
                    .addComponent(lblIntimidação))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CheckIntuição)
                    .addComponent(lblIntuição))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CheckLadinagem)
                    .addComponent(lblLadinagem))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CheckObterInformação)
                    .addComponent(lblObterInformação))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CheckOfício)
                    .addComponent(lblOfício))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CheckPercepção)
                    .addComponent(lblPercepção))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CheckSobrevivência)
                    .addComponent(lblSobrevivência))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 38, Short.MAX_VALUE)
                .addComponent(btmBack))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btmBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btmBackActionPerformed
        // TODO add your handling code here:
        FichaRPG back = new FichaRPG(personagem);
        back.setTitle("Ficha RPG Tormenta");
        back.setSize(674, 620);
        back.setVisible(true);
        dispose();
    }//GEN-LAST:event_btmBackActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[], Personagem personagem) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(NextView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(NextView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(NextView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(NextView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new NextView(personagem).setVisible(true);
            }
        });
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox CheckAcrobacia;
    private javax.swing.JCheckBox CheckAdestrar;
    private javax.swing.JCheckBox CheckAtletismo;
    private javax.swing.JCheckBox CheckAtuação;
    private javax.swing.JCheckBox CheckCavalgar;
    private javax.swing.JCheckBox CheckConhecimento;
    private javax.swing.JCheckBox CheckCura;
    private javax.swing.JCheckBox CheckDiplomacia;
    private javax.swing.JCheckBox CheckEnganação;
    private javax.swing.JCheckBox CheckFurtividade;
    private javax.swing.JCheckBox CheckIdentificar;
    private javax.swing.JCheckBox CheckIniciativa;
    private javax.swing.JCheckBox CheckIntimidação;
    private javax.swing.JCheckBox CheckIntuição;
    private javax.swing.JCheckBox CheckLadinagem;
    private javax.swing.JCheckBox CheckObterInformação;
    private javax.swing.JCheckBox CheckOfício;
    private javax.swing.JCheckBox CheckPercepção;
    private javax.swing.JCheckBox CheckSobrevivência;
    private javax.swing.JButton btmBack;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel lblAcrobacia;
    private javax.swing.JLabel lblAdestrar;
    private javax.swing.JLabel lblAtletismo;
    private javax.swing.JLabel lblAtuação;
    private javax.swing.JLabel lblCavalgar;
    private javax.swing.JLabel lblConhecimento;
    private javax.swing.JLabel lblCura;
    private javax.swing.JLabel lblDiplomacia;
    private javax.swing.JLabel lblEnganação;
    private javax.swing.JLabel lblFurtividade;
    private javax.swing.JLabel lblIdentificarMagia;
    private javax.swing.JLabel lblIniciativa;
    private javax.swing.JLabel lblIntimidação;
    private javax.swing.JLabel lblIntuição;
    private javax.swing.JLabel lblLadinagem;
    private javax.swing.JLabel lblObterInformação;
    private javax.swing.JLabel lblOfício;
    private javax.swing.JLabel lblPercepção;
    private javax.swing.JLabel lblSobrevivência;
    // End of variables declaration//GEN-END:variables
}
